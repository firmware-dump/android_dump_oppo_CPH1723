#!/system/bin/sh
if ! applypatch -c EMMC:/dev/block/platform/bootdevice/by-name/recovery:19121056:be0e1d34d4a88bf7eb46ba4d9fb9cbc6ca0a0bb8; then
  applypatch -b /system/etc/recovery-resource.dat EMMC:/dev/block/platform/bootdevice/by-name/boot:11807648:9f720ada732cc86ea108652dc6837225d99e0975 EMMC:/dev/block/platform/bootdevice/by-name/recovery be0e1d34d4a88bf7eb46ba4d9fb9cbc6ca0a0bb8 19121056 9f720ada732cc86ea108652dc6837225d99e0975:/system/recovery-from-boot.p && log -t recovery "Installing new recovery image: succeeded" || log -t recovery "Installing new recovery image: failed"
else
  log -t recovery "Recovery image already installed"
fi
